#include <assert.h>   
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>   
#include <X11/Xlib.h> 
#include <X11/Xatom.h>

void
set_window_props(Display *dpy, Window win) {
    /* setup window type and class */
    XChangeProperty(dpy, win,
        XInternAtom(dpy, "WM_CLASS", False), XA_STRING, 8, PropModeReplace,
        (unsigned char *) "fm", 2);

    XChangeProperty(dpy, win,
        XInternAtom(dpy, "WM_NAME", False), XA_STRING, 8, PropModeReplace,
        (unsigned char *) "find-mouse", 10);

    Atom xa = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_SPLASH", False);
    XChangeProperty(dpy, win, XInternAtom(dpy, "_NET_WM_WINDOW_TYPE", False),
        XA_ATOM, 32, PropModeReplace, (unsigned char*) &xa, 1);
}

void 
create_window(Display *dpy, Window *win, int x, int y, unsigned int side) {
    /* create the transparent, undecorated, no-border window 
     * centered at x and y and with side */
    XSetWindowAttributes attrib;
    attrib.override_redirect = True;
    attrib.background_pixmap = ParentRelative;
    attrib.border_pixel = 0;
    unsigned int half = side/2;
    *win = XCreateWindow(dpy, DefaultRootWindow(dpy), x-half, y-half, 
                         side, side,
                         0, CopyFromParent, InputOutput, CopyFromParent,
                         CWOverrideRedirect, &attrib);
    XSelectInput(dpy, *win, StructureNotifyMask);
    set_window_props(dpy, *win);
    XMapWindow(dpy, *win);
    for(;;) {
        XEvent ev;
        XNextEvent(dpy, &ev);
        if (ev.type == MapNotify) break;
    }
}

void
draw_cross(Display *dpy, Window win, GC gc, unsigned int side,
        char *bg, char *fg) {
    unsigned int half = side/2;
    unsigned int black_width = side/5;
    unsigned int white_width = side/20;

    /* set fill style */
    XSetFillStyle(dpy, gc, FillSolid);

    /* setup colors */
    XColor bg_col, fg_col;
    Colormap colormap;
    colormap = DefaultColormap(dpy, 0);

    XParseColor(dpy, colormap, bg, &bg_col);
    XAllocColor(dpy, colormap, &bg_col);
    XParseColor(dpy, colormap, fg, &fg_col);
    XAllocColor(dpy, colormap, &fg_col);

    /* draw the plus background */
    XSetForeground(dpy, gc, bg_col.pixel);
    XSetLineAttributes(dpy, gc, black_width, LineSolid, CapButt, JoinRound);
    XDrawLine(dpy, win, gc, half, 0, half, side);
    XDrawLine(dpy, win, gc, 0, half, side, half);
 
    /* draw the foreground */
    XSetForeground(dpy, gc, fg_col.pixel);
    XSetLineAttributes(dpy, gc, white_width, LineSolid, CapButt, JoinRound);
    XDrawLine(dpy, win, gc, half, 2, half, side-2);
    XDrawLine(dpy, win, gc, 2, half, side-2, half);
}

void 
fade_out(Display *dpy, Window win, double show_for_secs) {
    /* wait for half the time */
    long show_timeout =  (long) ((show_for_secs/2) * 1000000);
    usleep((useconds_t) show_timeout);
    /* change opacity till the delay runs out */
    double transparency = 1.0;
    int i=0;
    long fade_timeout = (long) (show_timeout / 100);
    for (; i<100; i++) { 
        unsigned int opacity = (unsigned int) (0xffffffff * transparency); 
        transparency -= .01;
        XChangeProperty(dpy, win, 
                XInternAtom(dpy, "_NET_WM_WINDOW_OPACITY", False),
                XA_CARDINAL, 32, PropModeReplace, (unsigned char *) &opacity, 1);
        XSync(dpy, False);
        usleep((useconds_t) fade_timeout);
    }
}

void 
read_opts(int argc, char **argv, unsigned int *side, 
        char **bg, char **fg, double *delay) {
    /* read options */
    int c;
    while ((c = getopt(argc, argv, "hs:f:b:d:")) != -1) {
        switch(c) {
            case 'h':
                fprintf(stdout, "usage: %s\n"
                        "\t-s <h|w of marker>\n"
                        "\t-f <foreground color>\n"
                        "\t-b <background color>\n"
                        "\t-d <delay in secs>\n",
                        argv[0]);
                exit(0);
            case 's': 
                *side = (unsigned int) atoi(optarg);
                if ((*side) % 2 != 0) {
                    fprintf(stderr, "Height|Width: %u needs to be even\n", *side);
                    exit(1);
                }
                break;
            case 'f':
                /* should be a macro and should check for hex */
                if (strlen(optarg) == 7 && optarg[0] == '#')
                    *fg = optarg;
                else {
                    fprintf(stderr, "Foreground: %s needs to be of the"
                            " form #FFFFFF\n", optarg);
                    exit(1);
                }
                break;
            case 'b':
                if (strlen(optarg) == 7 && optarg[0] == '#') 
                    *bg = optarg;
                else {
                    fprintf(stderr, "Foreground: %s needs to be of the"
                            " form #FFFFFF\n", optarg);
                    exit(1);
                }
                break;
            case 'd':
                *delay = atof(optarg);
            default:
                break;
        }
    }
}

int 
main (int argc, char **argv) {
    Display *dpy;
    Window root, win, rwin;
    GC gc;

    /* defaults to use in case users dont provide any */
    unsigned int side = 200;
    double delay = 1.0;
    char *bg = "#000000";
    char *fg = "#fafa87";

    /* read cli */
    read_opts(argc, argv, &side, &bg, &fg, &delay);

    /* plumbing - get display, root */
    dpy = XOpenDisplay(NULL);
    assert(dpy); // lazy ass !!
    root = DefaultRootWindow(dpy);

    /* get mouse location */
    int rx, ry, wx, wy;
    unsigned int mask;
    XQueryPointer(dpy, root, &root, &rwin, &rx, &ry, &wx, &wy, &mask);
    /* create window */
    create_window(dpy, &win, rx, ry, side);
    /* create gc */
    gc = XCreateGC(dpy, win, 0, NULL);
    /* draw the marker */
    draw_cross(dpy, win, gc, side, bg, fg);
    /* render window */
    XFlush(dpy);
    /* fade it out */
    fade_out(dpy, win, delay);
    /* cleanup */
    XFreeGC(dpy, gc);
    XCloseDisplay(dpy);

    return 0;
}
