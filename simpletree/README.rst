SimpleTree: A simple javascript object tree renderer
++++++++++++++++++++++++++++++++++++++++++++++++++++

.. contents:: Table of Contents

Overview
========

The motivation behind writing my own was to have a single javascript source that could render a list of objects or a single object without too much extra work. Most other libraries usually seem to require a secondary library and quite a bit of css. In some cases they even require a specific structure to the data provided to the renderer. I wanted something that was reasonably small, fast and just worked. 

SimpleTree tries to be as unobtrusive as possible while still providing quite a bit of flexibility to the developer in terms of looks, functionality and behaviour.

Features
========

* Minimal but still usable tree when not using any options.
* Custom overridable styles for most items in a tree.
* Auto generate ids for elements.
* Open tree and scroll to element if using a location hash fragment.
* Auto open or close trees on rendering.
* Deferred loaders for items.
* Allow HTML content.
* Hidden items.
* Custom animation handlers.

Usage
=====

To render a tree in its bare minimum configuration:

.. sourcecode:: javascript

    var st = SimpleTree("Tree Name", "id_of_container", data)

This will setup a blank tree with no header, inside the container specified. ``data`` can be an object, a list or even a literal. Although you would be better off using a plain list with literals. Look at example.html for detailed usage information.

The way a tree renders or behaves can be influenced by configuration options. Once a tree has been instantiated as shown above, you can then set up the configuration options using:

.. sourcecode:: javascript

    st.config.ui.option = value;

To render the tree you will need to call:

.. sourcecode:: javascript

    st.render();

Configuration
=============

The thing to keep in mind is that this was originally designed to handle a list of objects. Therefore there is a clear semantic difference between a **top-level** object or object member and everything else. A top level object or object member simply refers to the children of the root node in the tree.

Tree Options
------------

``config.tree.key``
    Key in a top-level object that can be used as a label for the object. This is only useful if you have a list of objects. If the data being rendered is an object, labels are already available. If not provided for a list of objects, the name of the tree with a default suffix is used as the label.

``config.tree.id_prefix``
    If element ids are required, this is the prefix that is used for the id. If ids are requests and this is not provided it falls back to using the container id and an underscore.

``config.tree.anchors``
    Request ids for elements. Allows you to traverse the tree using location hashes or otherwise link to specific parts of the tree. Defaults to false.

UI Options
----------

``config.ui.header``
    Show the header element. This uses the provided name to setup a header. Defaults to false.

``config.ui.html``
    Allows HTML content in the labels and values for tree nodes. Defaults to false.

.. Note:: This can have significant performance implications, so use it with care.

``config.ui.show``
    Custom method to open a tree node. This function will get a document element as an argument. For example you can use jQuery's ``slideDown`` method for this.

::
    
    config.ui.show = function(el) {
        $(el).slideDown();
    }

``config.ui.hide``
    Custom method to collapse a tree node.

``config.ui.hidden``
    Keys in top level objects that should be hidden. These keys are **not** rendered at all.

CSS Options
~~~~~~~~~~~

``config.ui.css.header``
    CSS class to apply to the header element if header is being shown.

``config.ui.css.closed``
    CSS class to apply to a collapsed tree node.

``config.ui.css.open``
    CSS class to apply to an open tree node.

``config.ui.css.list``
    CSS class for an UL element inside a collpsible node.

``config.ui.css.label``
    CSS class to apply to a label in the tree.

``config.ui.css.value``
    CSS class to apply to a value in the tree.

``config.ui.css.loader``
    CSS class to apply to a node that is being loaded in a delayed manner.

``config.ui.css.error``
    CSS class to apply to a node that had an error. So far only deferred loading failures use this.

``config.ui.css_cb``
    Callback function which can be passed in to provide additional css classes to add to a given label. The callback function is of the form:

.. sourcecode:: javascript
    function css_cb(name_of_key) {
        return additional_class;
    }



Loader Options
--------------

Loaders allow you to delay the loading of a node until the user has clicked on the node or to perform an external non-tree related action. To understand how to use loaders some explanation of the way SimpleTree keeps track of data is needed.

SimpleTree tracks nodes in a tree and their position in the original data passed in using a data attribute called ``path``. As it is rendering a tree it keeps track of what it is rendering in a list. So for example if an object can be accessed in the original data using

.. sourcecode:: javascript

    data[0].eplsilon.xi[0].pi

then the related node in the tree will look like

.. sourcecode:: html
        
    <div data-path='[0, "epsilon", "xi", 0, "pi"]'>...</div>

It is a straight JSON dump of the path to the node.

Loader functions are passed this object in when called. All loader functions have to be of the form

.. sourcecode:: javascript

    function(path, callback) {...}

The callback expects only one argument, the data to be loaded in. SimpleTree makes some assumptions about the argument that is passed in, in the following ways:

* If the argument is not provided, it assumes some sort of external action was taken and that it should keep the loader around so that it can be called again.
* If the argument is a string, it assumes that **an error occured** and the string is the error message. If ``config.loaders.retry`` is set, it will keep the loader around to reattempt the load action otherwise, it will revert to toggling the node.
* If the argument is an object or a list, it assumes that the argument is to be rendered underneath the node that request the load and renders it. It then resets the click event to toggle the node.

See ``example.js`` for how these are defined.

``config.loaders.base``
    The default loader to use for all deferred loads. If custom loaders are defined, this will be ignored.

``config.loaders.custom``
    Custom loaders keyed by the object labels that are used for loading specific nodes.

``config.loaders.retry``
    Flag to indicate whether to retry loading a node that encountered an error during a load. Defaults to true.
