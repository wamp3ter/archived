/* global window, document */
(function(global) {

    global.SimpleTree = function(name, container_id, data) {

        var config = {
            tree: {
                key: null,
                id_prefix: null,
                anchors: false
            },
            loaders: {
                base: null,
                custom: {},
                retry: true
            },
            ui: {
                header: false,
                html: false,
                show: null,
                hide: null,
                hidden: [],
                css: {},
                css_cb: null
            }
        };

        var loads = JSON.parse,
            dumps = JSON.stringify,
            win = window,
            doc = document,
            str = Object.prototype.toString,
            keys = Object.keys,
            idRegex = new RegExp(/[&<>]/g),
            tagsToReplace = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;"
            };

        // populated by setupXXX methods
        var st_classRegexes = {},
            st_keypath = [],
            st_cui,
            st_css,
            st_id_prefix,
            st_anchors,
            st_hidden,
            st_show,
            st_hide;

        function replaceTag(tag) {
            return tagsToReplace[tag] || tag;
        }

        function escapeHTML(s) {
            return s.replace(idRegex, replaceTag);
        }

        function convertToID(pathArray) {
            var path = pathArray.map(function(item) {
                item += "";
                return item.replace(/[^\w]/g, "_");
            });
            return st_id_prefix + path.join("_");
        }

        function collapsible(element, set) {
            if (set) {
                element.setAttribute("data-exp", true);
            }
            return element.hasAttributes() ?
                element.getAttribute("data-exp"): null;
        }

        function keypath(element, set) {
            if (set) {
                element.setAttribute("data-path", dumps(st_keypath));
                if (st_anchors) {
                    element.id = convertToID(st_keypath);
                }
            }
            return element.hasAttributes() ?
                element.getAttribute("data-path") : null;
        }

        function render(container, label, obj, children) {
            if (obj === null || obj === undefined) {
                obj = "-na-";
            }
            if (st_hidden.indexOf(label) < 0) {
                switch (str.call(obj)) {
                    case "[object Array]":
                        if (children) {
                            renderArrayChildren(container, obj);
                        }
                        else {
                            renderArray(container, label, obj);
                        }
                        break;
                    case "[object Object]":
                        if (children) {
                            renderObjectChildren(container, obj);
                        }
                        else {
                            renderObject(container, label, obj);
                        }
                        break;
                    default:
                        container = (container.tagName !== "UL") ?
                            addList(container) :
                            container;
                        var li = addElement(container, "li"),
                            le, ve;
                        if (label) {
                            le = addLabel(li, "span", label);
                        }
                        ve = addChild(li, "span", obj, "value");
                        checkAndSetDefaultStyle(ve, "value");
                        keypath(label? le : ve, true);
                }
            }
            st_keypath.pop();
        }

        function renderArray(container, label, arr) {
            container = (label) ?
                addCollapsible(container, label) :
                container;
            container = addList(container);
            if (label) {
                hide(container);
            }
            if (arr.length > 0) {
                renderArrayChildren(container, arr);
            }
            else {
                attachLoader(container, label);
            }
        }

        function renderArrayChildren(container, arr) {
            arr.map(function(item, idx) {
                st_keypath.push(idx);
                render(container, null, item);
            });
        }

        function renderObject(container, label, obj) {
            var key = config.tree.key;
            label = label ||
                    obj[key] ||
                    ((!label && st_keypath.length === 1 &&
                       typeof st_keypath[0] === "number") ?
                        label = name + ": Item #" + st_keypath[0] :
                        null);
            if (label) {
                container = addCollapsible(container, label);
                container = addList(container);
                hide(container);
            }
            else {
                container = addList(container);
            }
            if (keys(obj).length > 0) {
                renderObjectChildren(container, obj);
            }
            else {
                attachLoader(container, label);
            }
        }

        function renderObjectChildren(container, obj) {
            keys(obj).map(function(label) {
                st_keypath.push(label);
                render(container, label, obj[label]);
            });
        }

        function addCollapsible(container, label) {
            container = (container.tagName !== "UL") ?
                addList(container) :
                container;

            var li = addElement(container, "li", "closed");
            collapsible(li, true);
            var div = addLabel(li, "div", label);
            if (st_keypath.length) {
                keypath(div, true);
            }
            return li;
        }

        function addList(container) {
            var ul = addElement(container, "ul", "list");
            checkAndSetDefaultStyle(ul, "list");
            if (collapsible(ul.parentNode)) {
                ul.previousSibling.onclick = toggle;
            }
            return ul;
        }

        function addLabel(container, tag, label) {
            var el = addChild(container, tag, label, "label");
            checkAndSetDefaultStyle(el, "label");
            if (st_cui.css_cb) {
                var classes = st_cui.css_cb(label),
                    cls = el.className;
                if (classes && cls.indexOf(classes) < 0) {
                    cls += (cls ? " " : "") + classes;
                    el.className = cls;
                }
            }
            return el;
        }

        function addChild(container, tag, text, classes) {
            var element = addElement(container, tag),
                flag = config.ui.html;
            text = text.toString();
            if (text) {
                if (config.ui.html) {
                    element.innerHTML = text;
                }
                else {
                    element.textContent = escapeHTML(text);
                }
            }
            addClass(element, classes);
            return element;
        }

        function addElement(par, tag, classes) {
            var element = doc.createElement(tag);
            addClass(element, classes);
            par.appendChild(element);
            return element;
        }

        function show(element) {
            if (st_show) {
                st_show(element);
            }
            else {
                element.style.display = "block";
            }
            switchClassTo(element.parentNode, "open", "closed");
        }

        function hide(element) {
            if (st_hide) {
                st_hide(element);
            }
            else {
                element.style.display = "none";
            }
            switchClassTo(element.parentNode, "closed", "open");
        }

        function toggle(ev) {
            var element = ev.target.nextSibling;
            if (element.offsetWidth === 0 || element.offsetHeight === 0) {
                show(element);
            }
            else {
                hide(element);
            }
            ev.stopPropagation();
            return false;
        }

        function addClass(element, class_key) {
            var classes = st_css[class_key];
            if (classes)  {
                var cls = element.className;
                if (!cls) {
                    cls = classes;
                }
                else if (cls.indexOf(classes) < 0) {
                    cls += (cls ? " " : "") + classes;
                }
                element.className = cls;
            }
        }

        function removeClass(element, class_key) {
            var classes = st_css[class_key];
            var cls = element.className;
            if (classes && cls && cls.indexOf(classes) >= 0) {
                cls = cls.replace(st_classRegexes[class_key], "");
                element.className = cls;
            }
        }

        function switchClassTo(element, to, from) {
            if (from) {
                removeClass(element, from);
            }
            addClass(element, to);
        }

        function checkAndSetDefaultStyle(element, styleName) {
            if (!st_css[styleName] && !element.style.cssText) {
                switch (styleName) {
                    case "label":
                        if (element.tagName === "DIV") {
                            element.style.cssText += "font-weight: bold;cursor: pointer;";
                        }
                        /* falls through */
                    case "value":
                        element.style.cssText += "display:inline-block;padding:0.2em 1em;";
                        break;
                    case "list":
                        element.style.cssText = "list-style-type: none;";
                        break;
                }
            }
        }

        function getLoadCallback(ev) {
            return function(loaded_obj) {
                var target = ev.target,
                    label = target.textContent,
                    container = target.nextSibling;
                target.onclick = toggle;
                removeClass(target, "loader");
                if (!loaded_obj) {
                    target.onclick = loadObject;
                    return;
                }
                else if (str.call(loaded_obj) == "[object String]") {
                    addClass(container, "error");
                    if (config.loaders.retry) {
                        target.onclick = loadObject;
                    }
                    label = null;
                }
                render(container, label, loaded_obj, true);
                show(container);
            };
        }

        function loadObject(ev) {
            var target = ev.target;
            target.onclick = null;
            var container = target.nextSibling,
                path = loads(keypath(target)),
                label = target.textContent,
                cl = config.loaders,
                loader = cl.custom[label] || cl.base;
            hide(container);
            container.innerHTML = "";
            addClass(target, "loader");
            loader(path, getLoadCallback(ev));
        }

        function attachLoader(container, label) {
            var cl = config.loaders;
            if (cl.custom[label] || cl.base) {
                container.previousSibling.onclick = loadObject;
            }
            else {
                render(container, null, "-na-");
            }
        }

        function openAnchor() {
            var hash = win.location.hash;
            if (hash && hash.substr(1)) {
                hash = hash.substr(1);
                if (hash.substr(0, st_id_prefix.length) !== st_id_prefix) {
                    return;
                }
                var element = doc.getElementById(hash);
                if (element) {
                    while (element && element.parentNode) {
                        if (collapsible(element.parentNode)) {
                            show((element.tagName === "UL") ?
                                    element : element.nextSibling);
                            element.scrollIntoView();
                        }
                        element = element.parentNode;
                    }
                }
            }
        }

        function renderTree() {
            // setup variables
            st_cui = config.ui;
            st_css = st_cui.css;
            st_id_prefix = config.tree.id_prefix || (container_id + "_");
            st_hidden = st_cui.hidden;
            st_anchors = config.tree.anchors;
            st_show = st_cui.show;
            st_hide = st_cui.hide;
            // setup events
            if (st_anchors && "onhashchange" in win) {
                win.addEventListener("hashchange", openAnchor, false);
            }
            // setup regexps
            keys(st_css).map(function(key) {
                st_classRegexes[key] = key ? new RegExp("\\s*" + st_css[key]) : "";
            });
            // setup dummy container
            var container = doc.getElementById(container_id),
                fragment = doc.createDocumentFragment();
            if (name && st_cui.header) {
                addChild(fragment, "div", name, "header");
            }
            // render
            render(fragment, null, data);
            container.appendChild(fragment);
            openAnchor();
        }

        return {
            config: config,
            render: renderTree,
            getID: convertToID
        };
    };
})(this);
