var data = [
    {
        alpha: "Aleph One",
        id_: 1,
        gamma: true,
        delta: [ "No", "I" ],
        epsilon: {
            nu: 12987987e-4,
            xi: { omicron: "zoo" },
        },
        theta: "Kilgore Trout",
        "mappa mundi": null,
        phi: [ 1, undefined, null]
    },
    {
        alpha: "Aleph One",
        id_: 2,
        gamma: false,
        epsilon: {
            nu: 3,
            xi: { pi: "zoo" },
        },
        theta: "Rabo Karabekian",
        lambda: '<a href="http://www.google.com">Google</a>',
    },
];

var empty = {
    object_ok_list: {},
    object_ok_obj: {
        io: {}
    },
    object_err: {},
    "non tree action": {}
};

function show(el) {
    el.style.overflow = "hidden";
    el.style.display = "block";
    var h = el.offsetHeight;
    el.style.height = "0px";
    function increaseHeight() {
        if (el.offsetHeight < h) {
            var new_height = el.offsetHeight + 4;
            el.style.height = ((new_height > h) ? h : new_height) + "px";
            setTimeout(increaseHeight, 10);
        }
        else {
            el.style.removeProperty("height");
            el.style.removeProperty("overflow");
        }
    }
    increaseHeight();
}

function hide(el) {
    el.style.overflow = "hidden";
    el.style.display = "block";
    var h = el.offsetHeight;
    function reduceHeight() {
        if (el.offsetHeight > 0) {
            var new_height = el.offsetHeight - 4;
            el.style.height = ((new_height < 0) ? 0 : new_height) + "px";
            setTimeout(reduceHeight, 10);
        }
        else {
            el.style.display = "none";
            el.style.removeProperty("height");
            el.style.removeProperty("overflow");
        }
    }
    reduceHeight();
}

function runExamples() {
    var styles = {
        header: "header",
        closed: "collapsed",
        open: "open",
        list: "list",
        label: "label",
        value: "value",
        loader: "loader",
        error: "error"
    };

    // styled
    var st = SimpleTree("Styled + Default Label", "styled", data);
    st.config.tree.key = "theta";
    st.config.ui.header = true;
    st.config.ui.css = styles;
    st.render();

    // styled with css callback
    var st = SimpleTree("Styled + CSS Callback", "styled_css_cb", data);
    st.config.tree.key = "theta";
    st.config.ui.header = true;
    st.config.ui.css = styles;
    st.config.ui.css_cb = function(label) {
        if (label === "alpha") {
            return "label-extra";
        }
    }
    st.render();

    // styled with html
    st = SimpleTree("Styled + Object Only + HTML", "styled_w_html", data[1]);
    st.config.ui.css = styles;
    st.config.ui.header = true;
    st.config.ui.html = true;
    data[1]['<a href="http://google.com/rho">rho</a>'] = "rho no aware";
    st.render();
    delete data[1]['<a href="http://google.com/rho">rho</a>'];
    
    // external show hide
    st = SimpleTree("Styled + Custom Show/Hide", "ext_show_hide", data);
    st.config.tree.key = "theta";
    st.config.ui.css = styles;
    st.config.ui.header = true;
    st.config.ui.show = show;
    st.config.ui.hide = hide;
    st.render();
 
    // with anchors
    st = SimpleTree("Styled + Default Label + Anchors", "anchored", data);
    st.config.tree.key = "theta";
    st.config.tree.id_prefix = "st_";
    st.config.tree.anchors = true;
    st.config.ui.css = styles;
    st.config.ui.header = true;
    st.render();

    // hidden keys
    st = SimpleTree("Styled + Default Label + Hidden Keys", "hidden_keys", data);
    st.config.tree.key = "theta";
    st.config.ui.css = styles;
    st.config.ui.header = true;
    st.config.ui.hidden = ["id_", "gamma", "delta"];
    st.render();

    // with no loaders
    st = SimpleTree("Styled + Loadable + No Loader", "with_no_loader", 
            {"Move Along": []});
    st.config.ui.css = styles;
    st.config.ui.header = true;
    st.render();

    // with loaders
    st = SimpleTree("Styled + Loadable + Loader", "with_loader", empty);
    st.config.ui.css = styles;
    st.config.ui.header = true;
    st.config.loaders.base = function(keys, callback) {
        setTimeout(function() {
            callback(["We", "got", "something"]);
        }, 400);
    };
    st.config.loaders.custom.io = function(keys, callback) {
        setTimeout(function() {
            var obj = empty;
            for (var i=0,l=keys.length - 1; i<l; i++) {
                obj = obj[keys[i]];
            }
            callback({ alpha: 1, gamma: "wow" });
        }, 400);
    };
    st.config.loaders.custom.object_err = function(keys, callback) {
        setTimeout(function() {
            callback("Ouch! Something went bad...");
        }, 400);
    };
    st.config.loaders.custom["non tree action"] = function(keys, callback) {
        setTimeout(function() {
            var path = "empty";
            var value = empty;
            for (var i=0,l=keys.length; i<l; i++) {
                path += "[\"" + keys[i] + "\"]";
                value = value[keys[i]];
            }
            alert("Non-tree action\nName: " + path + "\nValue: " + value);
            callback();
        }, 400);
    };
    st.render();

    // unadorned object
    st = SimpleTree("Unadorned Object w/ Header", "unadorned_object", data[0]);
    st.config.ui.header = true;
    st.render();

    // unadorned list
    st = SimpleTree("Unadorned List w/o Header", "unadorned_list", data);
    st.render();

    // single element
    st = SimpleTree("Single Element", "single_el", "Lone String Element");
    st.render();

}
