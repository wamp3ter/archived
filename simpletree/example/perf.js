var data = {
    theta: "Kilgore Trout",
},
    count = 0;


function grs() {
    var res = "";
    var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        res += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
    return res;
}

function go(cd, d) {
    if (cd >= d) {
        return grc();
    }
    var ct = 0;
    var obj = {};
    var nc = 1 + Math.floor(Math.random() * 4);
    for (var i=0; i<nc; i++) {
        obj[grs()] = go(cd + 1, d);
        count++;
    }
    return obj;
}

function grc() {
    var opt = Math.floor(Math.random() * 2);
    var res = null;
    switch (opt) {
        case 0:
            res = [grs(), grs(), grs()];
            count += 3;
            break;
        case 1: 
            res = grs();
    }
    return res;
}

function glo() {
    var d = data;
    while (count < 100000) {
        var name = grs();
        var opt = Math.floor(Math.random() * 3);
        switch (opt) {
            case 0:
                d[name] = go(0, 5)
                break;
            case 1: //list
                d[name] = [grs(), grs(), grs()];
                count += 3;
                break;
            case 2: 
                d[name] = grs();
        }
        count++;
    }
}

function runperf() {
    var styles = {
        header: "header",
        closed: "collapsed",
        open: "open",
        list: "list",
        label: "label",
        value: "value",
        loader: "loader",
        error: "error"
    };


    glo();

    console.profile();

    // styled
    var st = SimpleTree("Perf", "perf", data);
    st.config.tree.key = "theta";
    st.config.ui.header = true;
    st.config.ui.css = styles;
    st.config.tree.id_prefix = "st_";
    st.config.tree.anchors = true;
    st.render();

    console.profileEnd();
}
