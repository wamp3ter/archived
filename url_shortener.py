#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cStringIO
import random
import socket
import struct
import sys
import time

MAP = map(chr, range(ord('a'), ord('z') + 1)) + \
      map(chr, range(ord('A'), ord('Z') + 1)) + \
      map(chr, range(ord('0'), ord('9') + 1))

def shorten(ip_str):
    ip  = struct.unpack('>I', socket.inet_aton(ip_str))[0]
    tm = int(time.time())
    num = ip ^ tm ^ (random.getrandbits(24) + (tm & 31))

    buf = cStringIO.StringIO()
    while num > 0:
        num, rem = divmod(num, 62)
        buf.write(MAP[rem])

    return buf.getvalue()

def generate_ips(max):
    for i in xrange(max):
        yield ".".join(str(random.randint(1, 255)) for i in range(4))

def test(max):
    u = set()
    dupes = 0
    print 'Starting run of %d shortens' % max
    sys.stdout.flush()
    idx = 0
    maxlen = 0
    e = 0
    for ip in generate_ips(max):
        s = time.time()
        r = shorten(ip)
        e += time.time() - s
        if len(r) > maxlen:
            maxlen = len(r)
        if r in u:
            dupes += 1
        idx += 1
        if idx % (max/100) == 0:
            print dupes, idx
    print 'Done'
    sys.stdout.flush()
    print 'Total time to create %d shorts: %f' % (max, e)
    print 'Maxlen: %d' % maxlen

max = 100000
if len(sys.argv) > 1:
    max = int(sys.argv[1])
test(max)

