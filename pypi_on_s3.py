#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import collections
import hashlib
import json
import os
import os.path as op
import re
import shutil
import subprocess as sp
import sys

import boto
from boto.s3.key import Key
from pkg_resources import Requirement

AWS_KEY = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET = os.environ.get('AWS_SECRET_ACCESS_KEY')

PACKAGE_RE = re.compile(r'-\d')

HTML_HEADER = ['<!doctype html>', '<html>', '\t<body>']
HTML_FOOTER = ['\t</body>', '</html>']

HERE = op.abspath(op.dirname(__file__))
INDEX_DIR = '_pypi'
PACKAGE_DIR = op.join(INDEX_DIR, 'packages')

S3_BUCKET = os.environ.get('AWS_PYPI_BUCKET')
S3_CONN = boto.connect_s3(aws_access_key_id=AWS_KEY,
                          aws_secret_access_key=AWS_SECRET)

def get_metadata_from_s3():
    bucket = S3_CONN.get_bucket(S3_BUCKET)
    key = bucket.get_key('metadata.json')
    if key:
        data = key.get_contents_as_string()
        return json.loads(data)
    return {}

def generate_metadata():
    print 'Generating package metadata'
    metadata = {}
    for filename in os.listdir(PACKAGE_DIR):
        filepath = op.join(PACKAGE_DIR, filename)
        if not op.isfile(filepath):
            print '%s is not a file. Removing' % filepath
            shutil.rmtree(filepath)
            continue
        md5 = hashlib.md5()
        with open(filepath) as fd:
            md5.update(fd.read())

        md5_tuple = (md5.hexdigest(), base64.b64encode(md5.digest()))
        package_name = PACKAGE_RE.split(filename, 1)[0].lower()
        final_dirname = op.join(PACKAGE_DIR, package_name[0], package_name)
        metadata.setdefault(package_name, {}).update({
            filename: {
                'current_path': filepath,
                'final_path': op.join(final_dirname, filename),
                'link_path': '%s#md5=%s' % (
                    op.join(final_dirname, filename).replace(INDEX_DIR, ''),
                    md5_tuple[0]),
                'md5': md5_tuple
            }
        })

    metadata = collections.OrderedDict(sorted(metadata.items()))
    return metadata

def get_version_from_filename(filename):
    version = filename.rstrip('.tar.gz')
    parts = version.split('-')
    if filename.endswith('egg'):
        version = parts[-2]
    else:
        version = parts[-1]
    return version

def generate_merged_metadata(metadata, remote_metadata):
    final = dict(remote_metadata)

    for package_name, files in metadata.items():
        if package_name not in final:
            final[package_name] = {}

        for filename, file_dict in files.items():
            if filename not in final[package_name]:
                final[package_name][filename] = {}
                version = get_version_from_filename(filename)
                final[package_name][filename]['version'] = version
                final[package_name][filename]['link_path'] = \
                    metadata[package_name][filename]['link_path']
                final[package_name][filename]['md5'] = \
                    metadata[package_name][filename]['md5'][0]

    return final

def generate_index_files(metadata):
    index_html = []
    index_html += HTML_HEADER
    index_html.append('\t\t<h2>s3.pypi</h2>')

    for package_name, package_files in metadata.items():
        print 'Generating link for %s' % package_name
        index_html.append('\t\t<div><a href="/%s">%s</a></div>' %
                (package_name, package_name))

        package_index_html = []
        package_index_html += HTML_HEADER
        package_index_html.append('\t\t<h2>s3.pypi: %s</h2>' % package_name)

        for filename, file_data in package_files.items():
            print 'Generating link for %s: %s' % (package_name, filename)
            package_index_html.append('\t\t<div><a href="%s">%s</a></div>' %
                (file_data['link_path'], filename))

        package_index_html += HTML_FOOTER
        print 'Writing package index %s' % package_name
        package_index_dir = op.join(INDEX_DIR, package_name)
        check_and_create_folder(package_name, package_index_dir)
        with open(op.join(package_index_dir, 'index.html'), 'w') as fd:
            fd.write('\n'.join(package_index_html))

    index_html += HTML_FOOTER
    print 'Generating simple index'
    with open(op.join(INDEX_DIR, 'index.html'), 'w') as fd:
        fd.write('\n'.join(index_html))

    print 'Generating metadata file'
    with open(op.join(INDEX_DIR, 'metadata.json'), 'w') as fd:
        json.dump(metadata, fd, indent=2, encoding='utf8')

def setup_package_structure(metadata):
    for package_name, package_data in metadata.items():
        for filename in package_data:
            file_data = package_data[filename]

            print 'Moving package file %s to %s' % (filename,
                    file_data['final_path'])
            package_file_dir = op.dirname(file_data['final_path'])
            check_and_create_folder(filename, package_file_dir)
            shutil.move(file_data['current_path'], file_data['final_path'])

def download_package(package_spec):
    print 'Using easy_install to download eggs for %s' % package_spec
    cmd = ['easy_install', '-zmaxqd', PACKAGE_DIR, package_spec]
    sp.check_call(cmd)

    print 'Using pip to download tarballs for %s' % package_spec
    cmd = ['pip', 'install', '--no-install', '-qd', PACKAGE_DIR, package_spec]
    sp.check_call(cmd)

def check_and_create_folder(name, path, remove_first=False):
    print 'Checking for %s folder' % name
    if remove_first and op.exists(path):
        print '%s folder found. Removing.' % name.title()
        shutil.rmtree(path)
    if not op.exists(path):
        print 'Creating %s folder' % name
        os.makedirs(path)

def upload_to_s3():
    bucket = S3_CONN.get_bucket(S3_BUCKET)
    for root, dir, files in os.walk(INDEX_DIR):
        for filename in files:
            path = op.join(root, filename).replace('%s/' % INDEX_DIR, '')
            print 'Uploading %s to S3' % path
            key = Key(bucket)
            key.key = path
            key.set_contents_from_filename(op.join(root, filename))
            key.set_acl('public-read')

def main():
    requirements_file = op.join(HERE, 'pypi_packages.txt')
    if len(sys.argv) > 1:
        requirements_file = sys.argv[1]

    requirements_file = op.abspath(requirements_file)
    if not op.exists(requirements_file):
        print 'ERROR: requirements file: %s not found' % requirements_file

    check_and_create_folder('index', INDEX_DIR, remove_first=True)
    check_and_create_folder('package', PACKAGE_DIR, remove_first=True)

    remote_metadata = get_metadata_from_s3()
    package_specs = []
    with open(requirements_file) as fd:
        for line in fd:
            line = line.strip().lower()
            if line and not line.startswith('#'):
                req = Requirement.parse(line)
                requirement_met = False
                if req.project_name in remote_metadata:
                    for filename in remote_metadata[req.project_name]:
                        version = get_version_from_filename(filename)
                        if version in req:
                            print 'Version %s in requirement %s' % (version,
                                req)
                            requirement_met = True
                            break
                if not requirement_met:
                    package_specs.append(line)

    for package_spec in package_specs:
        download_package(package_spec)

    metadata = generate_metadata()
    setup_package_structure(metadata)
    metadata = generate_merged_metadata(metadata, remote_metadata)
    generate_index_files(metadata)
    upload_to_s3()

if __name__ == '__main__':
    main()
