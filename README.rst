Miscellaneous Code
------------------

Stuff that I do not have a real home for.


``conky_cal.c``
    A small program to generate a center aligned calendar to be used with conky.

``generate-gnome-backpack.py``
    Generates the required xml file to use multiple images for gnome background that are cycled through.

``ThinBox.obt``
    An clean thin-edged openbox theme.

``fm.c``
    Mouse locater for X. Draws a user-defined plus sign.

``url_shortener.py``
    A fast reasonably collision free url shortener based on IP addresses.

``netwmpager``
    Archived version of a desktop pager with a few fixes.

``pypi_on_s3.py``
    A tool to create a custom pypi mirror on Amazon S3.
