#! /bin/bash

# TODO: trap all exits for cleanup

# setup
set -e
cd `dirname $0`

# make sure s3cmd exists and is configured
[ ! -f "`which s3cmd`" ] && echo 'Please install s3cmd' && exit 1
[ ! -f "$HOME/.s3cfg" ] && echo 'Run s3cmd --configure' && exit 2

if [[ "`uname`" != "Linux" && "`uname`" != "Darwin" ]]
then
    echo "You can only use this on OSX or Debian based systems"
    exit 1
fi

[ $# != 1 ] && echo "Please provide a sources location" && exit 1
[ ! -d $1 ] && echo "Source should be a folder" && exit 2

SOURCE_DIR=$1
TMPDIR=${TMPDIR:=/tmp}

# python script to walk the tree find setup.py(s) and list requirements
cat > $TMPDIR/generate_package_list.py <<EOF
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import mock
import setuptools

import os
import os.path as op
import sys

def find_all_setup_files():
    base_dir = op.abspath(os.environ.get('SOURCE_DIR', '..'))
    for root, dirs, files in os.walk(base_dir):
        for file in files:
            if file == 'setup.py' and '_install' not in root:
                yield root

def get_dependencies(path):
    print 'Processing: %s/setup.py' % path
    cur_dir = os.getcwd()
    os.chdir(path)
    deps = []
    with mock.patch.object(setuptools, 'setup') as mock_setup:
        try:
            import imp
            imp.load_source('setup', 'setup.py')
            args, kwargs = mock_setup.call_args
            deps = kwargs.get('install_requires', [])
            del sys.modules['setup']
        except ImportError as ex: # malformed package
            print 'WARNING: Malformed package at: %s' % path

    os.chdir(cur_dir)
    print 'Found: %d dependencies' % len(deps)
    return deps

results = set()
for setup_path in find_all_setup_files():
    print 'Found: %s' % setup_path
    results.update(get_dependencies(setup_path))

with open('_requirements.txt', 'w') as fd:
    results = list(results)
    results.sort()
    for result in results:
        fd.write('%s\n' % result)

EOF

# s3 bucket name to put stuf in
BUCKET_NAME=${BUCKET_NAME:=pypi}
BUCKET="s3://$BUCKET_NAME"

# setup work folders
WDIR=${WDIR:=wheelhouse}
SDIR=${SDIR:=_sdists}
PDIR=${PDIR:=_pypi}
mkdir -pv $SDIR $WDIR $PDIR

S3CMD="s3cmd -n"

# setup virtualenv and generate requirements list
virtualenv _install
source _install/bin/activate
pip install mock
python $TMPDIR/generate_package_list.py
deactivate
rm -rf _install

# download packages overwriting things as needed
pip install --upgrade setuptools
pip install wheel
for PKG in `cat _requirements.txt`
do
    pip wheel --download-cache $PDIR -f $WDIR -w $WDIR --exists-action=w $PKG
done

# put all wheel(s) on s3 right away
$S3CMD put -v -P --recursive ./$WDIR $BUCKET

# only put sources for packages which were not wheels
for PAIR in `ls $PDIR/*.{gz,zip,bz2} | awk '{ n=split($1,arr,"%2F"); print $1"|"arr[n]; }' | grep -v "content-type$"`
do
    FILE=`echo $PAIR | cut -d "|" -f 1`
    NEW_FILE=`echo $PAIR | cut -d "|" -f 2`
    mkdir -pv ./$SDIR
    mv -fv ./$FILE ./$SDIR/$NEW_FILE
    $S3CMD put -v -P ./$SDIR/$NEW_FILE $BUCKET/$WDIR/
done

# generate index list file
cat > index.html <<EOF
<html>
    <head>
        <title>Python Package Index</title>
    </head>
    <body>
EOF

for FILEPATH in `$S3CMD ls $BUCKET/$WDIR/ | awk '{print $4}'`
do
    FILENAME=`basename $FILEPATH`
    echo "    <a href=\"/$WDIR/$FILENAME\">$FILENAME</a><br/>"
    echo "    <a href=\"/$WDIR/$FILENAME\">$FILENAME</a><br/>" >> index.html
done

cat >> index.html <<EOF
    </body>
</html>
EOF

$S3CMD put -v -P index.html $BUCKET

# cleanup
rm -rf $PDIR $SDIR $WDIR index.html _requirements.txt $TMPDIR/generate_package_list.py

exit 0
