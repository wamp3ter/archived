#! /usr/bin/env python

import os
import os.path as op
import sys

OFN='background.xml'
DUR=1800.0
TR=4.0

if len(sys.argv) != 2:
    print 'usage: ' + sys.argv[0] + ' <folder with images>'
    sys.exit(1)

fldr = sys.argv[1]
if not op.exists(fldr):
    print 'Folder ' + fldr + ' not found'
    sys.exit(1)

files = os.listdir(fldr)
files = [f for f in files if op.splitext(f)[1].lower() in ('.jpg','.jpeg','.png')]
if len(files) == 0:
    print 'Could not find usable files.'
    sys.exit(0)

fd = open(op.join(fldr, OFN), 'w')
print >> fd, '''<background>
  <starttime>
    <year>2009</year>
    <month>08</month>
    <day>04</day>
    <hour>00</hour>
    <minute>00</minute>
    <second>00</second>
  </starttime>
  <!-- This animation will start at midnight. -->'''

first = files[0]
idx = 0
for fn in files:
    fpn = op.join(fldr, fn)
    if not op.exists(fpn):
        raise fpn + " does not exist"
    if idx != 0:
        print >> fd,'''
  <transition>
    <duration>''' + str(TR) + '''</duration>
    <from>''' + op.join(fldr, files[idx - 1]) + '''</from>
    <to>''' + fpn + '''</to>
  </transition>'''
    print >> fd, '''
  <static>
    <duration>''' + str(DUR) + '''</duration>
    <file>''' + fpn + '''</file>
  </static>'''
    idx += 1

print >> fd,'''
  <transition>
    <duration>''' + str(TR) + '''</duration>
    <from>''' + fpn + '''</from>
    <to>''' + op.join(fldr, first) + '''</to>
  </transition>'''


print >> fd, '''
</background>'''
fd.close()

sys.exit(0)
