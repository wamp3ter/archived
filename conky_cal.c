#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* TODO: 
 * - move globals in
 * - check args 
 */

int ALIGNC = 0;
int COLOR  = 0;

void
print_time(struct tm *tms) {

    char* DL[7] = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
    int MO[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int DT[42] = {
        -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1
    };

    /* check leap year and update Feb */
    if ((!((tms->tm_year)%4)) && (((tms->tm_year)%100) || (!((tms->tm_year)%400)))) 
        MO[1] = 29;

    /* print days */
    if (ALIGNC)
        printf("${alignc}");
    int i = 0;
    for (; i < 7; i++) {
        if (i == tms->tm_wday)
            printf("${color%d}%2s${color}", COLOR, DL[i]);
        else
            printf("%2s", DL[i]);
        if (i < 6)
            printf(" ");
    }
    printf("\n");

    /* skip till you get the first valid day */
    i = 0;
    /* find the wday of the first:
     * - find the offset from 1st day of week Su from today
     * - substract current week day to get Mo
     * - substract from 7 to get offset for 1
     */
    int wday_1 = 7 - ((tms->tm_mday % 7) - tms->tm_wday);
    if (wday_1 >= 7)
        wday_1 -= 7;
    /* 0 index - above result is the actual index, we need counts */
    wday_1 += 1;
    if (wday_1 < 7) {
        while (i < wday_1) 
            i++;
    }
    
    int j;
    for (j=1; j <= MO[tms->tm_mon]; i++,j++) 
        DT[i] = j;

    for (i=0; i<6; i++) {
        int flag = 0;
        if (ALIGNC)
            printf("${alignc}");
        for (j=0; j<7; j++) {
            int idx = (i*7) + j;
            if (DT[idx] > 0) {
                if (DT[idx] == tms->tm_mday)
                    printf("${color%d}%2d${color}", COLOR, DT[idx]);
                else
                    printf("%2d", DT[idx]);
                if (j != 6)
                    printf(" ");
            }
            /* skip empty lines at the end */
            else if (DT[idx] < 0 && i > 0 && j == 0) {
                flag = 1;
                break;
            }
            else {
                printf("  ");
                if (j != 6)
                    printf(" ");
            }
        }
        if (!flag)
            printf("\n");
    }
}

void setup_tm(struct tm *tms, const int yr, const int mo, 
        const int dy, const int wday) {
    tms->tm_year = yr -1900;
    tms->tm_mon = mo - 1;
    tms->tm_mday = dy;
    tms->tm_wday = wday - 1;
}

void
test() {
    struct tm tms;
    
    setup_tm(&tms, 2009, 2, 12, 5);
    print_time(&tms);
    
    setup_tm(&tms, 2009, 10, 12, 2);
    print_time(&tms);
    
    setup_tm(&tms, 2009, 11, 12, 5);
    print_time(&tms);
    
    setup_tm(&tms, 2009, 12, 12, 7);
    print_time(&tms);
    
    setup_tm(&tms, 2010, 2, 12, 6);
    print_time(&tms);
}
    
int
main(int argc, char *argv[]) 
{
#ifdef TEST
    test();
#else
    int opt;
    time_t cur_time;

    while ((opt = getopt(argc, argv, "ac:")) != -1) {
        switch (opt) {
            case 'a':
                ALIGNC = 1;
                break;
            case 'c':
                COLOR = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s [-a] [-c color_index]\n", argv[0]);
                return 1;
        }
    }

    cur_time = time(NULL);
    print_time(localtime(&cur_time));
#endif
    return 0;
}
