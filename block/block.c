/* See LICENSE file for license details. */
#define _XOPEN_SOURCE 500
#if HAVE_SHADOW_H
#include <shadow.h>
#endif

#include <ctype.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/vt.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <time.h>
#include <sys/select.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xrandr.h>

const char *FONTNAME = "-misc-fixed-*-*-*-*-15-*-*-*-*-*-*-*";
const int TICKS = 100;
const int MAX_ATTEMPTS = 5;
const int LOCKOUT = 300;

static void die(const char *errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	vfprintf(stderr, errstr, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

static const char *get_password()
{
#ifdef DEBUG
	return NULL;
#endif

	/* only run as root */
	if (geteuid() != 0)
		die("block: cannot retrieve password entry (make sure to"
		    " suid block)\n");
	struct passwd *pw = getpwuid(getuid());
	endpwent();
	const char *rval = pw->pw_passwd;

#if HAVE_SHADOW_H
	{
		struct spwd *sp;
		sp = getspnam(getenv("USER"));
		endspent();
		rval = sp->sp_pwdp;
	}
#endif

	/* drop privileges temporarily */
	if (setreuid(0, pw->pw_uid) == -1)
		die("block: cannot drop privileges\n");
	return rval;
}

struct ctnr {
	int screen, width, height, term, sw, sh, by, attempts, max_attempts;
	int lsx, lex, ticks;
	double interval;
	unsigned long white;
	const char *pws;
	char *dpy_name;
	Cursor invisible;
	Display *dpy;
	KeySym ksym;
	Pixmap pmap;
	Window root, w;
	XColor black, red, dummy;
	XFontStruct *font;
	GC ugc;
	GC lgc;
	GC pgc;
};

static void handle_args(int argc, char **argv, struct ctnr *c)
{
	int opt;
	opterr = 0;
	c->ticks = TICKS;
	c->dpy_name = NULL;
	c->attempts = 0;
	c->max_attempts = MAX_ATTEMPTS;

	while ((opt = getopt(argc, argv, "vt:d:")) != -1)
		switch (opt) {
		case 'v':
			die("block-" VERSION " © 2011 Wampeter Foma\n");
		case 't':
			c->ticks = strtol(optarg, NULL, 10);
			if (errno == EINVAL || errno == ERANGE)
				die("invalid value for -%s\n", optopt);
			if (c->ticks == 0)
				c->ticks = TICKS;
			c->ticks *= 10;
			break;
		case 'd':
			c->dpy_name = optarg;
			break;
		case '?':
			die("usage: block\n"
			    "\t-v\t\tprint version information\n"
			    "\t-t ticks\thow long to countdown in seconds [30]\n"
			    "\t-d display\twhich display to use [:0]\n");
		}
}

static void get_screen_size(struct ctnr *c)
{
	Rotation current_rotation;
	int nsize;
	XRRScreenConfiguration *sc = XRRGetScreenInfo(c->dpy, c->root);
	SizeID current_size = XRRConfigCurrentConfiguration(sc,
							    &current_rotation);
	XRRScreenSize *sizes = XRRConfigSizes(sc, &nsize);
	XRRFreeScreenConfigInfo(sc);
	for (int i = 0; i < nsize; i++) {
		if (i == current_size) {
			c->sw = sizes[0].width;
			c->sh = sizes[0].height;
			break;
		}
	}
	c->lsx = c->sw * 3 / 8;
	c->lex = c->sw * 5 / 8;
	c->interval = ((c->lex - c->lsx) * 1.0) / c->ticks;
}

static void daemonize()
{
#ifndef DEBUG
	int pid = fork();
	if (pid < 0)
		die("Could not fork block.");
	if (pid > 0)
		exit(0);	// exit parent 
#endif
}

static void create_window(struct ctnr *c)
{
	XSetWindowAttributes wa;
	wa.override_redirect = 1;
	wa.background_pixel = XBlackPixel(c->dpy, c->screen);
	c->w = XCreateWindow(c->dpy, c->root, 0, 0, c->width, c->height,
			     0, DefaultDepth(c->dpy, c->screen), CopyFromParent,
			     DefaultVisual(c->dpy, c->screen),
			     CWOverrideRedirect | CWBackPixel, &wa);

	XSelectInput(c->dpy, c->w, KeyPressMask | PointerMotionMask);

	// set a name & type for compiz opacity filters
	XChangeProperty(c->dpy, c->w,
			XInternAtom(c->dpy, "WM_NAME", False), XA_STRING, 8,
			PropModeReplace, (unsigned char *)"block", 6);

	Atom xa = XInternAtom(c->dpy, "_NET_WM_WINDOW_TYPE_NORMAL", False);
	XChangeProperty(c->dpy, c->w,
			XInternAtom(c->dpy, "_NET_WM_WINDOW_TYPE", False),
			XA_ATOM, 32, PropModeReplace, (unsigned char *)&xa, 1);

	unsigned int opacity = (unsigned int)(100 * 0xffffffff);
	XChangeProperty(c->dpy, c->w,
			XInternAtom(c->dpy, "_NET_WM_WINDOW_OPACITY", False),
			XA_CARDINAL, 32, PropModeReplace,
			(unsigned char *)&opacity, 1L);
}

static void allocate_colors(struct ctnr *c)
{
	XAllocNamedColor(c->dpy, DefaultColormap(c->dpy, c->screen),
			 "orange red", &c->red, &c->dummy);
	XAllocNamedColor(c->dpy, DefaultColormap(c->dpy, c->screen), "black",
			 &c->black, &c->dummy);
	c->white = XWhitePixel(c->dpy, c->screen);
}

static void create_cursor(struct ctnr *c)
{
	char curs[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	c->pmap = XCreateBitmapFromData(c->dpy, c->w, curs, 8, 8);
	c->invisible =
	    XCreatePixmapCursor(c->dpy, c->pmap, c->pmap, &c->black, &c->black,
				0, 0);
	XDefineCursor(c->dpy, c->w, c->invisible);
}

static void setup_fonts(struct ctnr *c)
{
	c->font = XLoadQueryFont(c->dpy, FONTNAME);
	if (c->font == 0) {
		die("error: could not find font. Try using a full description.\n");
	}

	int y = (c->sh + c->font->max_bounds.ascent -
		 c->font->max_bounds.descent) / 2;
	c->by = y - c->font->max_bounds.ascent;

	XGCValues values;
	c->ugc = XCreateGC(c->dpy, c->w, (unsigned long)0, &values);
	XSetFont(c->dpy, c->ugc, c->font->fid);
	XSetForeground(c->dpy, c->ugc, c->white);
	c->lgc = XCreateGC(c->dpy, c->w, (unsigned long)0, &values);
	XSetFont(c->dpy, c->lgc, c->font->fid);
	XSetForeground(c->dpy, c->lgc, c->white);
	c->pgc = XCreateGC(c->dpy, c->w, (unsigned long)0, &values);
	XSetFont(c->dpy, c->pgc, c->font->fid);
	XSetForeground(c->dpy, c->pgc, c->white);
}

static void lock_console(struct ctnr *c)
{
#ifndef DEBUG
	if ((ioctl(c->term, VT_LOCKSWITCH)) == -1) {
		perror("error locking console");
	}
#endif
}

static void unlock_console(struct ctnr *c)
{
#ifndef DEBUG
	if ((ioctl(c->term, VT_UNLOCKSWITCH)) == -1) {
		perror("error unlocking console");
	}
#endif
}

static void setup_locker(struct ctnr *c)
{
	c->pws = get_password();
	if (!(c->dpy = XOpenDisplay(c->dpy_name)))
		die("block: cannot open dpy\n");
	c->screen = DefaultScreen(c->dpy);
	c->root = RootWindow(c->dpy, c->screen);
	c->width = DisplayWidth(c->dpy, c->screen);
	c->height = DisplayHeight(c->dpy, c->screen);
	c->sw = c->width;
	c->sh = c->height;
	get_screen_size(c);
	allocate_colors(c);
	create_window(c);
	create_cursor(c);
	setup_fonts(c);
}

static Bool grab_pointer_keyboard(struct ctnr *c)
{
	unsigned int len = 0;
	Bool no_pass_match = True;
	for (len = 1000; len; len--) {
		if (XGrabPointer
		    (c->dpy, c->root, False,
		     ButtonPressMask | ButtonReleaseMask | PointerMotionMask,
		     GrabModeAsync, GrabModeAsync, None, c->invisible,
		     CurrentTime) == GrabSuccess)
			break;
		usleep(1000);
	}
	if ((no_pass_match = no_pass_match && (len > 0))) {
		for (len = 1000; len; len--) {
			if (XGrabKeyboard
			    (c->dpy, c->root, True, GrabModeAsync,
			     GrabModeAsync, CurrentTime) == GrabSuccess)
				break;
			usleep(1000);
		}
		no_pass_match = (len > 0);
	}
	return no_pass_match;
}

static void render_attempts(struct ctnr *c)
{
	XFillRectangle(c->dpy, c->w, c->ugc,
		       (c->sw - c->font->max_bounds.width * 9) / 2, c->by - 28,
		       c->font->max_bounds.width * 9,
		       c->font->max_bounds.ascent - 2);
}

static void render_line(struct ctnr *c, int count)
{
	XSetForeground(c->dpy, c->lgc, c->white);
	XDrawLine(c->dpy, c->w, c->lgc, c->lsx, c->by - 10, c->lex, c->by - 10);
	XSetForeground(c->dpy, c->lgc, c->red.pixel);
	XDrawLine(c->dpy, c->w, c->lgc,
		  c->lex - (count * c->interval), c->by - 10, c->lex,
		  c->by - 10);
}

static void render_password(struct ctnr *c, int len)
{
	XFillRectangle(c->dpy, c->w, c->pgc,
		       (c->sw - c->font->max_bounds.width * len) / 2, c->by,
		       c->font->max_bounds.width * len,
		       c->font->max_bounds.ascent - 2);
}

static void render(struct ctnr *c, int len, int count)
{
	// sometimes on resume xrandr is a bit lost so redo it
	// if that is the case
	if (c->interval == 0)
		get_screen_size(c);
	XClearWindow(c->dpy, c->w);
	render_attempts(c);
	render_line(c, count);
	render_password(c, len);
}

static void cleanup(struct ctnr *c)
{
	setreuid(geteuid(), 0);
	unlock_console(c);
	close(c->term);
	setuid(getuid());	// drop rights permanently

	XUngrabPointer(c->dpy, CurrentTime);
	XFreePixmap(c->dpy, c->pmap);
	XFreeFont(c->dpy, c->font);
	XFreeGC(c->dpy, c->ugc);
	XFreeGC(c->dpy, c->lgc);
	XFreeGC(c->dpy, c->pgc);
	XDestroyWindow(c->dpy, c->w);
	XCloseDisplay(c->dpy);
}

int main(int argc, char **argv)
{
	struct ctnr c;

	handle_args(argc, argv, &c);
#ifndef DEBUG
	if ((c.term = open("/dev/console", O_RDWR)) == -1) {
		perror("error opening console");
	}
#endif
	lock_console(&c);
	daemonize();
	setup_locker(&c);
	Bool no_pass_match = grab_pointer_keyboard(&c);
	XMapRaised(c.dpy, c.w);
	XSync(c.dpy, False);

	char buf[32], passwd[256];
	unsigned int len = 0;
	int update = False;
	// get x11 fd to run select on
	int fd = ConnectionNumber(c.dpy);
	int count = 0;
	fd_set read_fds;
	XActivateScreenSaver(c.dpy);
	// main event loop 
	while (no_pass_match) {
		// countdown is done or we should not update reset count and sleep
		if (count >= c.ticks || !update) {
			count = 0;
			XActivateScreenSaver(c.dpy);
		}
		// Create a File Description Set containing fd
		FD_ZERO(&read_fds);
		FD_SET(fd, &read_fds);

		struct timeval tv;
		memset(&tv, 0, sizeof(tv));
		// Set our ticks.  A tenth of second sounds good.
		tv.tv_usec = 100000;
		tv.tv_sec = 0;

		int res = select(fd + 1, &read_fds, 0, 0, &tv);
		if (res == 0) {	
			// if we should update we update countdown and render
			if (update) {
				count++;
				render(&c, len, count);
				XFlush(c.dpy);
			} else // else reset countdown
				count = 0;
		} else if (res < 0) {	// interrupted! retry
			if (errno == EINTR)
				continue;
			die("Unknown select error: %d", errno);
		} else {
			// Handle XEvents and flush the input 
			while (XPending(c.dpy)) {
				XEvent ev;
				XNextEvent(c.dpy, &ev);

				// event happened - render and set update to True
				count++;
				render(&c, len, count);
				update = True;

				// dont deal with MotionNotify as it merely wakes up the
				// screen
				if (ev.type == KeyPress) {
					XSetForeground(c.dpy, c.ugc, c.white);

					buf[0] = 0;
					int num = XLookupString(&ev.xkey, buf,
								sizeof buf,
								&c.ksym,
								0);
					if (IsKeypadKey(c.ksym)) {
						if (c.ksym == XK_KP_Enter)
							c.ksym = XK_Return;
						else if (c.ksym >= XK_KP_0
							 && c.ksym <= XK_KP_9)
							c.ksym =
							    (c.ksym - XK_KP_0) +
							    XK_0;
					}
					if (IsFunctionKey(c.ksym)
					    || IsKeypadKey(c.ksym)
					    || IsMiscFunctionKey(c.ksym)
					    || IsPFKey(c.ksym)
					    || IsPrivateKeypadKey(c.ksym))
						continue;

					switch (c.ksym) {
					case XK_Return:
						passwd[len] = 0;
#ifndef DEBUG
						no_pass_match =
						    strcmp(crypt(passwd, c.pws), c.pws);
#else
						no_pass_match = len;
#endif
						if (no_pass_match != 0)
							// change text color on wrong password
							XSetForeground(c.dpy, c.ugc, c.red. pixel);

						len = 0;
						break;
					case XK_Escape:
						len = 0;
						update = False;
						break;
					case XK_BackSpace:
						if (len)
							--len;
						break;
					default:
						if (num && !iscntrl((int)buf[0])
						    && (len + num <
							sizeof passwd)) {
							memcpy(passwd + len, buf, num);
							len += num;
						}

						break;
					}
				}
			}
		}
	}

	/* free and unlock */
	cleanup(&c);
	return 0;
}
