``block`` - simple screen locker
================================
Simple screen locker utility for X, based on slock. Provides a very basic user feedback.   

Requirements
------------
In order to build block you need the Xlib, Xext and Xrandr header files.

Building
--------
You need to run::

    make clean all

To build in debug mode::

    make clean debug

.. note:: In debug mode, hitting anything other than CR is assumed to be an error state.

Installation
------------
Manual installation:
Edit config.mk to match your local setup (block is installed into the /usr/local namespace by default).

Afterwards enter the following command to build and install block
(if necessary as root)::

    make clean install

Running ``block``
-----------------
Simply invoking the ``block`` or ``xlock`` command starts the display locker with default settings.

Notes
-----

Forked from https://github.com/benruijl/sflock

Major differences are:

- Update this document to reflect current state of things
- Add WM_NAME so compiz can set opacity properly
- Change error mode to switch font color and not the entire background
- Update Makefile so xlock link is setup, so that xflock4 can work
- Render rectangles instead of characters
- Remove options to specify font and no line
- Deal with multi-monitor setup via XRandR
- Deal with DPMS properly
- Remove BSD stuff
- Add animated countdown
- Add proper debug support
- Add countdown timer command line argument
- Simplified rendering logic

.. note:: By now, other than the core Xlib calls pretty much all of it has been rewritten.
